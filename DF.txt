#Its dockerfile

FROM python:3.8
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app

ADD . /usr/src/app

ENTRYPOINT ["python", "/usr/src/app/script.py"]