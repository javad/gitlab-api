import gitlabfuncs
from flask import Flask, request
from flask_restful import Resource, Api
import subprocess , sys
import gitlab
from base64 import b64encode
import psycopg2
import modify_db as db


app = Flask(__name__)
api = Api(app)

'''

@app.route('/register', methods=["POST"])
def handle_register():
    username = request.json["username"]
    email = request.json["email"]
    password = request.json["password"]
    db.register(username, password, email)
    return "OK"



@app.route('/login', methods=["POST"])
def handle_login():
    username = request.json["username"]
    password = request.json["password"]
    token = db.login(username, password)
    return token


# We can use the command below to each function we need authentication
#user_id = authenticate(token)

'''

class AllProjects(Resource):
    def get(self):
        return {'data': gitlabfuncs.get_all_projects()}

api.add_resource(AllProjects, '/api/allprojects/')


@app.route('/api/createproject/',methods=["POST"])
def Create_Project():
    name = request.json['name']
    project_id,res = gitlabfuncs.create_project(name)
    db.add_project(name,project_id)
    return { 'data' : res }



@app.route('/api/createcommit/', methods=["GET","POST"])   
def Create_Commit():
    repo_id = str(request.json['repo'])
    repo = gitlabfuncs.get_gitlab_project(repo_id)
    new_content = """import time 

while 1:
    print('{}')
    time.sleep(1)""".format(request.json['new_content'])
    commit_message = request.json['commit_message']
    gitlabfuncs.create_commit('script.py', 'master', commit_message, new_content, repo)
    return { "data": "ok"}



@app.route('/api/pushfile/',methods=["GET","POST"])
def Push_File():
    project_id = str(request.json['project_id'])
    file_path = request.json['file_path']
    branch = request.json['branch']
    content = request.json['content']
    commit_message = request.json['commit_message']
    gitlabfuncs.create_file(project_id,file_path,branch,content,commit_message)
    project_url = gitlabfuncs.return_url(project_id)
    ## call bash command ##
    subprocess.call('sh {} tmp/bashscript.sh --args'.format(project_url), shell=True)
    return {"data": "It\'s done"}




if __name__ == '__main__':
     app.run()
