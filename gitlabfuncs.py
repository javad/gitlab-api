import gitlab
from base64 import b64encode
import os
from dotenv import load_dotenv


load_dotenv()

PRIVATE_TOKEN = os.getenv('PRIVATE_TOKEN')
GITLAB_INSTANCE = os.getenv('GITLAB_INSTANCE')



def get_all_projects():
    gl = gitlab.Gitlab(GITLAB_INSTANCE, private_token=PRIVATE_TOKEN)
    projects = gl.http_list('/projects',owned=True)
    return projects


def get_gitlab_project(project_id):

    gl = gitlab.Gitlab(GITLAB_INSTANCE, private_token=PRIVATE_TOKEN)
    project = gl.projects.get(project_id)
    if not project:
        raise 'Could not get Project "{0}" from GitLab API.'.format(project_id)
    
    return project 
    

    
def create_commit(path, branch, commit_message, content, project):

    f = project.files.get(file_path=path, ref=branch)
    f.content = b64encode(content.encode()).decode()
    f.encoding = 'base64'
    f.save(branch=branch, commit_message=commit_message) 



def delete_project(project_id):
    gl = gitlab.Gitlab(GITLAB_INSTANCE, private_token=PRIVATE_TOKEN)
    gl.http_delete('/projects/{project_id}')


def create_file(project_id,file_path,branch,content,commit_message):
    gl = gitlab.Gitlab(GITLAB_INSTANCE, private_token=PRIVATE_TOKEN)
    project = gl.projects.get(project_id)
    project.files.create({'file_path': file_path,
                          'branch': branch,
                          'content': img2b64(content),
                          'author_email': 'javadnikbakht@aol.com',
                          'author_name': 'Javad Nikbakht',
                          'encoding': 'base64',
                          'commit_message': commit_message
                          })



def create_project(project_name='kuknos_test'):
    gl = gitlab.Gitlab(GITLAB_INSTANCE, private_token=PRIVATE_TOKEN)
    project = gl.projects.create({'name': project_name , 'visibility':'public'})
    project_id = project.id 
    project.files.create({'file_path': 'Dockerfile',
                          'branch': 'master',
                          'content': img2b64('DF.txt'),
                          'author_email': 'javadnikbakht@aol.com',
                          'author_name': 'Javad Nikbakht',
                          'encoding': 'base64',
                          'commit_message': "Pushing Dockerfile of this project"
                          })

    project.files.create({'file_path': 'script.py',
                          'branch': 'master',
                          'content': img2b64('script.py'),
                          'author_email': 'javadnikbakht@aol.com',
                          'author_name': 'Javad Nikbakht',
                          'encoding': 'base64',
                          'commit_message': "Pushing script.py for this project"
                          })
    return project_id,"The project created successfully."


def get_projects_names_ids():
    gl = gitlab.Gitlab(GITLAB_INSTANCE, private_token=PRIVATE_TOKEN)
    projects = gl.http_list('/projects',owned=True)
    projectsList = []
    for p in projects:
        projectsList.append((p['name'],p['id']))

    return projectsList


def img2b64(fileaddress='testfile.txt'):
    img_file = open(fileaddress, 'rb')
    b64 = b64encode(img_file.read())
    return b64.decode()


def return_url(project_id):
    gl = gitlab.Gitlab(GITLAB_INSTANCE, private_token=PRIVATE_TOKEN)
    projects = gl.http_list('/projects',owned=True)
    for p in projects:
        if p['id'] == project_id:
            return p['web_url']+'.git'
        else:
            continue
